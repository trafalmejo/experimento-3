package com.monitoreo.servicios;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class SeguridadAES {

	/**
	 * Algoritmo para la encriptacion junto con las especificaciones de padding
	 */
	public static final String ALGORITMO = "AES/ECB/PKCS5Padding";
	/**
	 * Llave simetrica para desencriptar
	 */
	private SecretKeySpec key;
	
	/**
	 * Constructor que se encarga de incializar la llave
	 * @param llave String en formato UTF-8
	 * @throws UnsupportedEncodingException si la llave no esta en el formato establecido
	 */
	public SeguridadAES(String llave) throws UnsupportedEncodingException{
		byte[] keyb = llave.getBytes("UTF-8");
		key = new SecretKeySpec(keyb, "AES");
	}
	
	/**
	 * Metodo que se encarga de encriptar un mensaje y dejarlo en numeros hex
	 * @param mensaje texto plano con el mensaje a enviar, debe estar en formato UTf-8
	 * @return un string con el hexagecimal correspondiente al texto encriptado
	 * @throws Exception si algo sale mal
	 */
	public String encriptar (String mensaje)
			throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, 
			NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException 
	{	
		byte[] msg = mensaje.getBytes("UTF-8");
		Cipher decifrador = Cipher.getInstance(ALGORITMO); 
		decifrador.init(Cipher.ENCRYPT_MODE, key); 
		return aHex(decifrador.doFinal(msg));
	}
	
	/**
	 * Metodo que recibe un texto encriptado y en hex y lo pasa a texto plano
	 * @param encriptado texto encriptado y en hex para convertir
	 * @return un String con el texto plano
	 * @throws Exception si algo sale mal
	 */
	public String desencriptar (String encriptado)
			throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, 
			NoSuchAlgorithmException, NoSuchPaddingException 
	{
		Cipher decifrador = Cipher.getInstance(ALGORITMO); 
		decifrador.init(Cipher.DECRYPT_MODE, key); 
		byte[] bmsg = aPlano(encriptado);
		return new String(decifrador.doFinal(bmsg));
	}
	
	private byte[] aPlano( String plano)
	{
		byte[] ret = new byte[plano.length()/2];
		for (int i = 0 ; i < ret.length ; i++) {
			ret[i] = (byte) Integer.parseInt(plano.substring(i*2,(i+1)*2), 16);
		}
		return ret;
	}
	
	private String aHex( byte[] bytes )
	{
		String ret = "";
		for (int i = 0 ; i < bytes.length ; i++) {
			String g = Integer.toHexString(((char)bytes[i])&0x00ff);
			ret += (g.length()==1?"0":"") + g;
		}
		return ret;
	}
	
	/*public static void main(String[] args) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException {
		String s = "sacalo, gozalo..";
		String msg = "mensaje a encriptar, no estoy seguro de que esto solo tenga 16 caracteres\n"
				+"Pero vale la pena intentarlo porque la vida es muy corta y no se cuando se vaya a acabar\n"
				+"por ejemplo, hoy me puedo caer y pum! me mori."; 
		
		SeguridadAES saes = new SeguridadAES(s);
		
		String resp = saes.encriptar(msg);
		System.out.println(resp);
		String orig = saes.desencriptar(resp);
		System.out.println(orig);
		String m = "4adcbf02f3d65ad9c28e79efc7fd402c8b3386ffc06d5235b67fe0e2b075198d887ca8e8e4bb84a6"
				+"0762b5c99dcbed0836c1f8de7c60c5ec45777bb3c29347adfa38237afaaad062fa37e5d9825ec8f5"
				+"810f66354b8c4fc7adcb6281fdd5903c89c05989dcdaefb6da57e5480b097068ce58125e24b8044e"
				+"a103efd25fa24b8fde9b8a65ed8ffe50fd775d9698445a1c956a66530f4a314c8e13fa204474d8de"
				+"8f670bab9d686d57f7e5a3b652652a4b0e5803a21b020cde2e0c631101c98355e9180b9638a663ff"
				+"3ce0ddaff8f8ecc3897676e1f45e2bfd34eeedf7c833028fe8db7bf3570f16e75ffc38fc35944be3"
				+"c1775df5a7e27a6c821d353c4f369364";
		System.out.println(m.equals(resp));
		orig = saes.desencriptar(m);
		System.out.println(orig);

	}*/


}
