/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.monitoreo.servicios;

import com.monitoreo.entidades.Dieta;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Andres Decastro
 */
@Stateless
public class DietaFacade extends AbstractFacade<Dieta> {
    @PersistenceContext(unitName = "Monitoreo-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DietaFacade() {
        super(Dieta.class);
    }
    
}
