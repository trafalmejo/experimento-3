/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.monitoreo.servicios;

import com.monitoreo.entidades.Presion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Andres Decastro
 */
@Stateless
public class PresionFacade extends AbstractFacade<Presion> {
    @PersistenceContext(unitName = "Monitoreo-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PresionFacade() {
        super(Presion.class);
    }
    
}
