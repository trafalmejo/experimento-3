/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.monitoreo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Andres Decastro
 */
@Entity
@Table(name = "DIETA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dieta.findAll", query = "SELECT d FROM Dieta d"),
    @NamedQuery(name = "Dieta.findByDescripcion", query = "SELECT d FROM Dieta d WHERE d.descripcion = :descripcion"),
    @NamedQuery(name = "Dieta.findByDuraciondias", query = "SELECT d FROM Dieta d WHERE d.duraciondias = :duraciondias"),
    @NamedQuery(name = "Dieta.findByNombre", query = "SELECT d FROM Dieta d WHERE d.nombre = :nombre"),
    @NamedQuery(name = "Dieta.findByDietaid", query = "SELECT d FROM Dieta d WHERE d.dietaid = :dietaid")})
public class Dieta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Size(max = 255)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DURACIONDIAS")
    private BigDecimal duraciondias;
    @Size(max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DIETAID")
    private BigDecimal dietaid;
    @OneToMany(mappedBy = "dietaid")
    private Collection<Imc> imcCollection;

    public Dieta() {
    }

    public Dieta(BigDecimal dietaid) {
        this.dietaid = dietaid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getDuraciondias() {
        return duraciondias;
    }

    public void setDuraciondias(BigDecimal duraciondias) {
        this.duraciondias = duraciondias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getDietaid() {
        return dietaid;
    }

    public void setDietaid(BigDecimal dietaid) {
        this.dietaid = dietaid;
    }

    @XmlTransient
    public Collection<Imc> getImcCollection() {
        return imcCollection;
    }

    public void setImcCollection(Collection<Imc> imcCollection) {
        this.imcCollection = imcCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dietaid != null ? dietaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dieta)) {
            return false;
        }
        Dieta other = (Dieta) object;
        if ((this.dietaid == null && other.dietaid != null) || (this.dietaid != null && !this.dietaid.equals(other.dietaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.monitoreo.entidades.Dieta[ dietaid=" + dietaid + " ]";
    }
    
}
