/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.monitoreo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Andres Decastro
 */
@Entity
@Table(name = "IMC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Imc.findAll", query = "SELECT i FROM Imc i"),
    @NamedQuery(name = "Imc.findByActual", query = "SELECT i FROM Imc i WHERE i.actual = :actual"),
    @NamedQuery(name = "Imc.findByDiagnostico", query = "SELECT i FROM Imc i WHERE i.diagnostico = :diagnostico"),
    @NamedQuery(name = "Imc.findByFechamedicion", query = "SELECT i FROM Imc i WHERE i.fechamedicion = :fechamedicion"),
    @NamedQuery(name = "Imc.findByImc", query = "SELECT i FROM Imc i WHERE i.imc = :imc"),
    @NamedQuery(name = "Imc.findByImcid", query = "SELECT i FROM Imc i WHERE i.imcid = :imcid")})
public class Imc implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "ACTUAL")
    private Character actual;
    @Size(max = 50)
    @Column(name = "DIAGNOSTICO")
    private String diagnostico;
    @Column(name = "FECHAMEDICION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechamedicion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "IMC")
    private BigDecimal imc;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IMCID")
    private BigDecimal imcid;
    @JoinColumn(name = "PACIENTEID", referencedColumnName = "PACIENTEID")
    @ManyToOne
    private Paciente pacienteid;
    @JoinColumn(name = "DIETAID", referencedColumnName = "DIETAID")
    @ManyToOne
    private Dieta dietaid;

    public Imc() {
    }

    public Imc(BigDecimal imcid) {
        this.imcid = imcid;
    }

    public Character getActual() {
        return actual;
    }

    public void setActual(Character actual) {
        this.actual = actual;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public Date getFechamedicion() {
        return fechamedicion;
    }

    public void setFechamedicion(Date fechamedicion) {
        this.fechamedicion = fechamedicion;
    }

    public BigDecimal getImc() {
        return imc;
    }

    public void setImc(BigDecimal imc) {
        this.imc = imc;
    }

    public BigDecimal getImcid() {
        return imcid;
    }

    public void setImcid(BigDecimal imcid) {
        this.imcid = imcid;
    }

    public Paciente getPacienteid() {
        return pacienteid;
    }

    public void setPacienteid(Paciente pacienteid) {
        this.pacienteid = pacienteid;
    }

    public Dieta getDietaid() {
        return dietaid;
    }

    public void setDietaid(Dieta dietaid) {
        this.dietaid = dietaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imcid != null ? imcid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Imc)) {
            return false;
        }
        Imc other = (Imc) object;
        if ((this.imcid == null && other.imcid != null) || (this.imcid != null && !this.imcid.equals(other.imcid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.monitoreo.entidades.Imc[ imcid=" + imcid + " ]";
    }
    
}
