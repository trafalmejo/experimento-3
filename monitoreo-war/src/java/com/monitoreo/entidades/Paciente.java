/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.monitoreo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Andres Decastro
 */
@Entity
@Table(name = "PACIENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Paciente.findAll", query = "SELECT p FROM Paciente p"),
    @NamedQuery(name = "Paciente.findByApellidos", query = "SELECT p FROM Paciente p WHERE p.apellidos = :apellidos"),
    @NamedQuery(name = "Paciente.findByEstaturaactualcms", query = "SELECT p FROM Paciente p WHERE p.estaturaactualcms = :estaturaactualcms"),
    @NamedQuery(name = "Paciente.findByFechanacimiento", query = "SELECT p FROM Paciente p WHERE p.fechanacimiento = :fechanacimiento"),
    @NamedQuery(name = "Paciente.findByGenero", query = "SELECT p FROM Paciente p WHERE p.genero = :genero"),
    @NamedQuery(name = "Paciente.findByNombre", query = "SELECT p FROM Paciente p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Paciente.findByTipodocumento", query = "SELECT p FROM Paciente p WHERE p.tipodocumento = :tipodocumento"),
    @NamedQuery(name = "Paciente.findByPacienteid", query = "SELECT p FROM Paciente p WHERE p.pacienteid = :pacienteid")})
public class Paciente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Size(max = 50)
    @Column(name = "APELLIDOS")
    private String apellidos;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ESTATURAACTUALCMS")
    private BigDecimal estaturaactualcms;
    @Column(name = "FECHANACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechanacimiento;
    @Size(max = 1)
    @Column(name = "GENERO")
    private String genero;
    @Size(max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    @Lob
    @Size(max = 0)
    @Column(name = "NUMERODOCUMENTO")
    private String numerodocumento;
    @Size(max = 2)
    @Column(name = "TIPODOCUMENTO")
    private String tipodocumento;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PACIENTEID")
    private BigDecimal pacienteid;
    @JoinColumn(name = "MEDICOID", referencedColumnName = "MEDICOID")
    @ManyToOne
    private Medico medicoid;
    @OneToMany(mappedBy = "pacienteid")
    private Collection<Imc> imcCollection;
    @OneToMany(mappedBy = "pacienteid")
    private Collection<Presion> presionCollection;
    @OneToMany(mappedBy = "pacienteid")
    private Collection<Aviso> avisoCollection;

    public Paciente() {
    }

    public Paciente(BigDecimal pacienteid) {
        this.pacienteid = pacienteid;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public BigDecimal getEstaturaactualcms() {
        return estaturaactualcms;
    }

    public void setEstaturaactualcms(BigDecimal estaturaactualcms) {
        this.estaturaactualcms = estaturaactualcms;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumerodocumento() {
        return numerodocumento;
    }

    public void setNumerodocumento(String numerodocumento) {
        this.numerodocumento = numerodocumento;
    }

    public String getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(String tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public BigDecimal getPacienteid() {
        return pacienteid;
    }

    public void setPacienteid(BigDecimal pacienteid) {
        this.pacienteid = pacienteid;
    }

    public Medico getMedicoid() {
        return medicoid;
    }

    public void setMedicoid(Medico medicoid) {
        this.medicoid = medicoid;
    }

    @XmlTransient
    public Collection<Imc> getImcCollection() {
        return imcCollection;
    }

    public void setImcCollection(Collection<Imc> imcCollection) {
        this.imcCollection = imcCollection;
    }

    @XmlTransient
    public Collection<Presion> getPresionCollection() {
        return presionCollection;
    }

    public void setPresionCollection(Collection<Presion> presionCollection) {
        this.presionCollection = presionCollection;
    }

    @XmlTransient
    public Collection<Aviso> getAvisoCollection() {
        return avisoCollection;
    }

    public void setAvisoCollection(Collection<Aviso> avisoCollection) {
        this.avisoCollection = avisoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pacienteid != null ? pacienteid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paciente)) {
            return false;
        }
        Paciente other = (Paciente) object;
        if ((this.pacienteid == null && other.pacienteid != null) || (this.pacienteid != null && !this.pacienteid.equals(other.pacienteid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.monitoreo.entidades.Paciente[ pacienteid=" + pacienteid + " ]";
    }
    
}
