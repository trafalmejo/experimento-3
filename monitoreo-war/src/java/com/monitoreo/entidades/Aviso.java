/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.monitoreo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Andres Decastro
 */
@Entity
@Table(name = "AVISO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aviso.findAll", query = "SELECT a FROM Aviso a"),
    @NamedQuery(name = "Aviso.findByFechaaviso", query = "SELECT a FROM Aviso a WHERE a.fechaaviso = :fechaaviso"),
    @NamedQuery(name = "Aviso.findByMensaje", query = "SELECT a FROM Aviso a WHERE a.mensaje = :mensaje"),
    @NamedQuery(name = "Aviso.findByTipo", query = "SELECT a FROM Aviso a WHERE a.tipo = :tipo"),
    @NamedQuery(name = "Aviso.findByTitulo", query = "SELECT a FROM Aviso a WHERE a.titulo = :titulo"),
    @NamedQuery(name = "Aviso.findByAvisoid", query = "SELECT a FROM Aviso a WHERE a.avisoid = :avisoid")})
public class Aviso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "FECHAAVISO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaaviso;
    @Size(max = 50)
    @Column(name = "MENSAJE")
    private String mensaje;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TIPO")
    private BigDecimal tipo;
    @Size(max = 50)
    @Column(name = "TITULO")
    private String titulo;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AVISOID")
    private BigDecimal avisoid;
    @JoinColumn(name = "PACIENTEID", referencedColumnName = "PACIENTEID")
    @ManyToOne
    private Paciente pacienteid;

    public Aviso() {
    }

    public Aviso(BigDecimal avisoid) {
        this.avisoid = avisoid;
    }

    public Date getFechaaviso() {
        return fechaaviso;
    }

    public void setFechaaviso(Date fechaaviso) {
        this.fechaaviso = fechaaviso;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public BigDecimal getTipo() {
        return tipo;
    }

    public void setTipo(BigDecimal tipo) {
        this.tipo = tipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public BigDecimal getAvisoid() {
        return avisoid;
    }

    public void setAvisoid(BigDecimal avisoid) {
        this.avisoid = avisoid;
    }

    public Paciente getPacienteid() {
        return pacienteid;
    }

    public void setPacienteid(Paciente pacienteid) {
        this.pacienteid = pacienteid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (avisoid != null ? avisoid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aviso)) {
            return false;
        }
        Aviso other = (Aviso) object;
        if ((this.avisoid == null && other.avisoid != null) || (this.avisoid != null && !this.avisoid.equals(other.avisoid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.monitoreo.entidades.Aviso[ avisoid=" + avisoid + " ]";
    }
    
}
