/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.monitoreo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Andres Decastro
 */
@Entity
@Table(name = "PRESION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Presion.findAll", query = "SELECT p FROM Presion p"),
    @NamedQuery(name = "Presion.findByActual", query = "SELECT p FROM Presion p WHERE p.actual = :actual"),
    @NamedQuery(name = "Presion.findByDia", query = "SELECT p FROM Presion p WHERE p.dia = :dia"),
    @NamedQuery(name = "Presion.findByDiastolica", query = "SELECT p FROM Presion p WHERE p.diastolica = :diastolica"),
    @NamedQuery(name = "Presion.findByFechamedicion", query = "SELECT p FROM Presion p WHERE p.fechamedicion = :fechamedicion"),
    @NamedQuery(name = "Presion.findBySistolica", query = "SELECT p FROM Presion p WHERE p.sistolica = :sistolica"),
    @NamedQuery(name = "Presion.findByPresionid", query = "SELECT p FROM Presion p WHERE p.presionid = :presionid")})
public class Presion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "ACTUAL")
    private Character actual;
    @Column(name = "DIA")
    private Character dia;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DIASTOLICA")
    private BigDecimal diastolica;
    @Column(name = "FECHAMEDICION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechamedicion;
    @Column(name = "SISTOLICA")
    private BigDecimal sistolica;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRESIONID")
    private BigDecimal presionid;
    @JoinColumn(name = "PACIENTEID", referencedColumnName = "PACIENTEID")
    @ManyToOne
    private Paciente pacienteid;

    public Presion() {
    }

    public Presion(BigDecimal presionid) {
        this.presionid = presionid;
    }

    public Character getActual() {
        return actual;
    }

    public void setActual(Character actual) {
        this.actual = actual;
    }

    public Character getDia() {
        return dia;
    }

    public void setDia(Character dia) {
        this.dia = dia;
    }

    public BigDecimal getDiastolica() {
        return diastolica;
    }

    public void setDiastolica(BigDecimal diastolica) {
        this.diastolica = diastolica;
    }

    public Date getFechamedicion() {
        return fechamedicion;
    }

    public void setFechamedicion(Date fechamedicion) {
        this.fechamedicion = fechamedicion;
    }

    public BigDecimal getSistolica() {
        return sistolica;
    }

    public void setSistolica(BigDecimal sistolica) {
        this.sistolica = sistolica;
    }

    public BigDecimal getPresionid() {
        return presionid;
    }

    public void setPresionid(BigDecimal presionid) {
        this.presionid = presionid;
    }

    public Paciente getPacienteid() {
        return pacienteid;
    }

    public void setPacienteid(Paciente pacienteid) {
        this.pacienteid = pacienteid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (presionid != null ? presionid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Presion)) {
            return false;
        }
        Presion other = (Presion) object;
        if ((this.presionid == null && other.presionid != null) || (this.presionid != null && !this.presionid.equals(other.presionid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.monitoreo.entidades.Presion[ presionid=" + presionid + " ]";
    }
    
}
